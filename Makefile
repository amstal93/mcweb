

# Project configuration
# ------------------------------


PROJECT = ccosweb

CMS_IMG = wordpress
CMS_TAG = 4.9.4-php7.2-apache

DB_IMG = mariadb
DB_TAG = 10.3.3

PORT = 8080

# Build rules
# ------------------------------

DC_FLAGS = -p $(PROJECT)

src = src
compose_file = docker-compose.yml
conf_files = cms.conf db.conf
setup_files = $(patsubst %, $(src)/%, $(conf_files)) .env

docker_v1 = 18
docker_compose_v1 = 1
docker_compose_v2 = 17

# Note flags

note_cms_conf=0
note_db_conf=0

# Default rule

all:
	@echo "Available user targets:"
	@echo
	@echo "  up         build and start application"
	@echo "  down       terminate application"
	@echo "  rmvol      erase volumes (and stop application)"
	@echo "  rmimg      erase images (and stop applications)"
	@echo "  rmall      erase everithing (rmvol, rmimg etc.)"
	@echo "  status     show information about application"
	@echo "  clean      erase temporary files, as usual"
	@echo "  delconf    delete configuration files (reset)"


# Create conf-files from templates

$(src)/cms.conf: lib/cms_$(CMS_IMG).conf
	cp $< src/cms.conf
	$(eval note_cms_conf=1)

$(src)/db.conf: lib/db_$(DB_IMG).conf
	cp $< src/db.conf
	$(eval note_db_conf=1)

.env:
	echo "CMS_IMG=$(CMS_IMG)" >> $@
	echo "CMS_TAG=$(CMS_TAG)" >> $@
	echo "DB_IMG=$(DB_IMG)" >> $@
	echo "DB_TAG=$(DB_TAG)" >> $@
	echo "PORT=$(PORT)" >> $@
	cat lib/cms_$(CMS_IMG).env >> $@
	cat lib/db_$(DB_IMG).env >> $@


# User rules

.PHONE: up down rmvol rmi rmall status delconf backup

up: $(setup_files) .config
	docker-compose $(DC_FLAGS) -f $(src)/$(compose_file) up -d
	$(call notes)

down: $(setup_files) .config
	docker-compose $(DC_FLAGS) -f $(src)/$(compose_file) down 

rmvol:$(setup_files) .config
	docker-compose $(DC_FLAGS) -f $(src)/$(compose_file) down --volume

rmi: .config
	make down
	docker rmi --force $(CMS_IMG):$(CMS_TAG)
	docker rmi --force $(DB_IMG):$(DB_TAG)

rmall: .config
	make rmi
	make rmvol

# docker run -it --volumes-from <container_with_volume> -v <host_dir>:/<container_dir> --name <smth> ubuntu /bin/bash


# backup:
# 	@$(eval vol=$(shell grep CMS_VOL .env))
# 	docker run --rm --volumes-from ccosweb_cms_1 -v $$(pwd)/bkp:/bkp ubuntu tar cvf bkp/cms_backup.tar $(patsubst CMS_VOL=%,%,$(vol))
# 	@$(eval vol=$(shell grep DB_VOL .env))
# 	docker run --rm --volumes-from ccosweb_db_1 -v $$(pwd)/bkp:/bkp ubuntu tar cvf bkp/db_backup.tar $(patsubst DB_VOL=%,%,$(vol))

# restore:
# 	@$(eval vol=$(shell grep CMS_VOL .env))
# 	docker run --rm --volumes-from ccosweb_cms_1 -v $$(pwd)/bkp:/bkp ubuntu tar xvf bkp/cms_backup.tar #$(patsubst CMS_VOL=%,%,$(vol))
# 	@$(eval vol=$(shell grep DB_VOL .env))
# 	docker run --rm --volumes-from ccosweb_db_1 -v $$(pwd)/bkp:/bkp ubuntu tar xvf bkp/db_backup.tar #$(patsubst DB_VOL=%,%,$(vol))


backup:
	@$(eval vol=$(shell grep CMS_VOL .env))
	@$(eval path=$(patsubst CMS_VOL=%,%,$(vol)))
	docker run --rm --volumes-from ccosweb_cms_1 -v $$(pwd)/bkp:/bkp ubuntu tar zcvf /bkp/cms_backup.tar $(path)
	@$(eval vol=$(shell grep DB_VOL .env))
	@$(eval path=$(patsubst DB_VOL=%,%,$(vol)))
	docker run --rm --volumes-from ccosweb_db_1 -v $$(pwd)/bkp:/bkp ubuntu tar zcvf /bkp/db_backup.tar $(path)

restore:
#	@$(eval vol=$(shell grep CMS_VOL .env))
#	@$(eval path=$(patsubst CMS_VOL=%,%,$(vol)))
	docker run --rm --volumes-from ccosweb_cms_1 -v $$(pwd)/bkp:/bkp ubuntu tar zxvf bkp/cms_backup.tar /
#	@$(eval vol=$(shell grep DB_VOL .env))
#	@$(eval path=$(patsubst DB_VOL=%,%,$(vol)))
	docker run --rm --volumes-from ccosweb_db_1 -v $$(pwd)/bkp:/bkp ubuntu tar zxvf bkp/db_backup.tar /

delconf: 
	rm -f $(patsubst %, $(src)/%, cms.conf db.conf) .env .config

status: .config
	@echo
	@echo ============================= CONTAINERS =======================================
	@echo
	@docker-compose $(DC_FLAGS) -f $(src)/$(compose_file) ps
	@echo
	@docker-compose $(DC_FLAGS) -f $(src)/$(compose_file) images
	@echo
	@echo =============================== VOLUMES ========================================
	@echo
	@docker volume ls --filter "name=$(PROJECT)"
	@echo
	@echo =============================== IMAGES =========================================
	@echo
	@docker images 
	@echo

clean:
	rm -f *~ \#*

# Check check dependencies if there's not .config as yet.

.config:
	@echo -n "Checking configuration..."
	$(call check_deps)
	@echo "ok" > $@
	@echo "ok."

# Post-build notes to the user


define notes
	@if test $(note_cms_conf) -eq 1 ; then\
	  echo; \
	  echo "File $(src)/cms.conf was created from $(CMS_IMG) template.";\
	  echo "You should edit it manually to complete your local setup.";\
	  echo "Some ajustments may be needed to accomodate version changes.";\
	fi
	@if test $(note_db_conf) -eq 1 ; then\
	  echo; \
	  echo "File $(src)/db.conf was created from $(DB_IMG) template.";\
	  echo "You should edit it manually to complete your local setup.";\
	  echo "Some ajustments may be needed to accomodate version changes.";\
	fi
	@echo
endef


# Check dependencies (docker and docker-compose required versions)

define check_deps

	@if ! test -f $$(which docker) ; then \
          echo "*** Required program 'docker' missing."; \
	  echo "    Please, install version $(docker_v1) and retry.";\
         exit 1; fi
	@ v1=$$(docker --version | sed "s/\(.*\)version \([^.]*\).*/\2/") && \
	if test $$v1 -lt $(docker_v1); then\
	  echo "*** $$(docker --version) found; minimal version required is $(docker_v1)";\
	  echo "    Please upgrade and retry"; exit 1;fi


	@if ! test -f $$(which docker-compose) ; then \
          echo "*** Required program 'docker-compose' missing."; \
	  echo "    Please, install version $(docker_compose_required_version) and retry.";\
	exit 1; fi

	@ v1=$$(docker-compose --version | sed "s/\(.*\)version \([^.]*\).\([^.]*\).*/\2/");\
	  v2=$$(docker-compose --version | sed "s/\(.*\)version \([^.]*\).\([^.]*\).*/\3/");\
	  if ! (test $$v1 -ge $(docker_compose_v1) && test $$v2 -ge $(docker_compose_v2)); then\
	    echo "*** $$(docker-compose --version) found; minimal version required is $(docker_compose_v1).$(docker_compose_v2)";\
	    echo "    Please upgrade and retry"; exit 1; fi

endef

